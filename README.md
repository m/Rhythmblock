![Rhythmblock](logo.png)
# Rhythmblock

a graphical, customizable music player for CC: Tweaked

![A ComputerCraft computer running Rhythmblock](screenshot.png)

## Features

- easily customizable  (just edit the file and change whatever colors you want directly from the variables)
- compatible with all placeable computers and all monitors
- keyboard support
- eject discs directly from the player
- status bar

## Keyboard Shortcuts

space - play/stop

e - eject

q - quit

## Install

run the following command on the desired CC computer

`wget https://fem.mint.lgbt/m/Rhythmblock/raw/branch/master/rhythmblock.lua Rhythmblock`
